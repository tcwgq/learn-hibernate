package com.tcwgq.p_hbm_extends;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

public class App {
	private static SessionFactory sessionFactory = null;
	static {
		sessionFactory = new Configuration().configure()
				.addClass(Article.class).buildSessionFactory();
	}

	// 保存，有关联关系
	@Test
	public void testSave() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Article article = new Article();
		article.setTitle("这是一个Article");
		Topic topic = new Topic();
		topic.setTitle("这是一个Topic");
		Reply reply = new Reply();
		reply.setTitle("这是一个Reply");
		session.save(article);
		session.save(topic);
		session.save(reply);
		session.getTransaction().commit();
		session.close();
	}

	// 获取，可以获取到关联的对方
	@Test
	public void testGet() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		Article article = (Article) session.get(Article.class, 1);
		Topic topic = (Topic) session.get(Topic.class, 2);
		Reply reply = (Reply) session.get(Reply.class, 3);
		System.out.println(article);
		System.out.println(topic);
		System.out.println(reply);
		System.out.println("-----------------");
		// 可以通过多态的方式获得子类对象
		Article topic1 = (Article) session.get(Article.class, 2);
		System.out.println(topic1);
		Article reply1 = (Article) session.get(Article.class, 3);
		System.out.println(reply1);
		ts.commit();
		session.close();
	}

}
