package com.tcwgq.k_session_api;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

/**
 * 只有get方法是在调用之后马上执行sql语句，其他的都是在调用flush语句之后才执行sql语句
 * 
 * @author lenovo
 * 
 */
public class App {
	private static SessionFactory sessionFactory = null;
	static {
		sessionFactory = new Configuration().configure().addClass(User.class)
				.buildSessionFactory();
	}

	// save，把临时状态转变为持久化状态
	@Test
	public void testSave() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		User user = new User();// 临时状态
		user.setName("张三");// 临时状态
		session.save(user);// 持久化状态，insert语句在xxxx之前执行
		session.save(user);// 不管save多少次，只插入一次
		user.setName("李四");// 持久化状态，数据库中同步，update语句在xxxxxx之后执行
		System.out.println("xxxxxxx");
		session.getTransaction().commit();
		session.close();
	}

	// 获取数据，是持久化状态
	// 会生成select语句
	// 马上执行sql语句
	// 如果对象不存在，就会生成null
	@Test
	public void testGet() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		User user = (User) session.get(User.class, 1);
		System.out.println(user.getClass());
		System.out.println("------------");
		ts.commit();
		session.close();
		System.out.println(user);// 游离状态
	}

	@Test
	public void testUpdate() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		User user = (User) session.get(User.class, 1);
		System.out.println(user);// 持久化状态
		user.setName("test");
		session.flush();// 刷出到数据库
		// session.clear();// 清除session中所有对象
		session.evict(user);// 清除session中指定对象
		session.update(user);// 持久化状态
		session.getTransaction().commit();// 默认提交的时候才提交到数据库
		session.close();
	}

	// 把临时状态或游离状态转变为持久化状态
	// 会生成insert或update语句
	// 在更新时，对象不存在就报错
	// 本方法是根据ID判断对象是什么状态的，ID为原始值就是临时状态，如果不是原始值就是游离状态
	@Test
	public void testSaveOrUpdate() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		User user = new User();// 临时状态
		user.setName("张三");
		session.saveOrUpdate(user);// 持久化
		session.getTransaction().commit();
		session.close();
	}

	// 把持久化或游离转变为删除状态
	// 会生成delete
	// 如果删除的对象不存在，就会跑出异常
	@Test
	public void testDelete() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		User user = new User();
		user.setId(2);
		user.setName("张三");
		session.delete(user);
		System.out.println("--------------");
		session.getTransaction().commit();
		session.close();
	}

	// load，获取数据，是持久化状态
	// 会生成select语句
	// 不会马上执行sql语句，而是在第一次使用非ID或class属性时执行sql语句
	// load后返回的是一个代理对象，要求类不能是final的，否则就不能生成子类代理，就不能使用懒加载功能了。
	// 让懒加载失效的方式：1.设置类为final属性 2.xml中配置lazy属性为false
	// 如果数据不存在，就跑出ObjectNotFoundException
	@Test
	public void testLoad() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		User user = (User) session.load(User.class, 100);
		System.out.println(user.getId());
		System.out.println(user.getClass());
		System.out.println("--------------");
		session.getTransaction().commit();
		session.close();
	}

	// 操纵大量数据时，要防止session中对象过多而内存溢出
	@Test
	public void testBatchSave() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		for (int i = 0; i < 1000; i++) {
			User user = new User();
			user.setName("batch");
			session.save(user);
			if (i % 10 == 0) {
				// 清空session之前一定要flush一下
				session.flush();
				session.clear();
			}
		}
		session.getTransaction().commit();
		session.close();
	}

	@Test
	public void testGet1() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		User user = (User) session.get(User.class, 1);
		System.out.println(user.getName());
		ts.commit();
		session.close();
	}

}
