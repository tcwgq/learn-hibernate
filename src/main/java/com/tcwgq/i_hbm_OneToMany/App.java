package com.tcwgq.i_hbm_OneToMany;

import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

public class App {
	private static SessionFactory sessionFactory = null;
	static {
		sessionFactory = new Configuration().configure()
				.addClass(Department.class).addClass(Employee.class)
				.buildSessionFactory();
	}

	// 保存，有关联关系
	@Test
	public void testSave() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Department department = new Department();
		department.setName("研发部");
		Employee e1 = new Employee();
		e1.setName("zhangSan");
		e1.setDepartment(department);
		Employee e2 = new Employee();
		e2.setName("liSi");
		e2.setDepartment(department);
		department.getEmployees().add(e1);
		department.getEmployees().add(e2);
		// 被依赖的一方要先保存
		session.save(department);
		// session.save(e1);
		// session.save(e2);
		session.getTransaction().commit();
		session.close();
	}

	// 获取，可以获取到关联的对方
	@Test
	public void testGet() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		// 获取一方
		Department department = (Department) session.get(Department.class, 1);
		System.out.println(department.getName());
		// 显示另一方的信息
		Set<Employee> set = department.getEmployees();
		for (Employee emp : set) {
			int id = emp.getId();
			String name = emp.getName();
			String dept = emp.getDepartment().getName();
			System.out.println(id + "-" + name + "-" + dept);
		}
		ts.commit();
		session.close();
	}

	@Test
	public void testRemoveRelation() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		// 从员工方解除
		Employee emp = (Employee) session.get(Employee.class, 1);
		emp.setDepartment(null);
		// 从部门方解除，与inverse有关系，为false时可解除
		Department dept = (Department) session.get(Department.class, 1);
		dept.getEmployees().clear();
		ts.commit();
		session.close();
	}

	@Test
	public void testDelete() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		// 删除员工方，对对方没有影响
		// Employee emp = (Employee) session.get(Employee.class, 1);
		// session.delete(emp);
		// 删除部门方
		// 如果没有关联的员工，能删除
		// 如果有关联的员工且inverse=true，由于不能维护关联关系，所以会直接执行删除，就会报异常。
		// 如果有关联的员工且inverse=false，由于可以维护关联关系，就会先把关联的员工的外键列设置为null，再删除自己。
		Department dept = (Department) session.get(Department.class, 1);
		session.delete(dept);
		ts.commit();
		session.close();
	}

	@Test
	public void testLazy() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		Department dept = (Department) session.get(Department.class, 1);
		System.out.println(dept.getName());
		Hibernate.initialize(dept.getEmployees());// 立即加载指定打懒加载对象
		ts.commit();
		session.close();
		// 在使用懒加载时，要注意懒加载异常
		System.out.println(dept.getEmployees().size());
	}
}
