package com.tcwgq.d_utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {
	private static SessionFactory sessionFactory = null;
	static {
		// Configuration cfg = new Configuration();
		// cfg.configure();// 读取默认的配置文件，配置文件为hibernate.cfg.xml，且放在src目录下
		// cfg.configure("hibernate.cfg.xml");// 读取指定的配置文件
		// cfg.addResource("com/tcwgq/domain/User.hbm.xml");// 手动添加映射文件
		// cfg.addClass(User.class);//
		// sessionFactory = cfg.buildSessionFactory();
		sessionFactory = new Configuration()// 去User类所在的包中，查找后缀为hbm.xml的文件
				.configure()// 强制格式化
				.buildSessionFactory();// 链式编程
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static Session openSession() {
		return sessionFactory.openSession();
	}
}
