package com.tcwgq.a_domain;

import java.util.List;

public class QueryResult {
	private int counts;
	private List<User> list;

	public QueryResult() {
		super();
	}

	public QueryResult(int counts, List<User> list) {
		super();
		this.counts = counts;
		this.list = list;
	}

	public int getCounts() {
		return counts;
	}

	public void setCounts(int counts) {
		this.counts = counts;
	}

	public List<User> getList() {
		return list;
	}

	public void setList(List<User> list) {
		this.list = list;
	}

}
