package com.tcwgq.j_hbm_ManyToMany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

public class App {
	private static SessionFactory sessionFactory = null;
	static {
		sessionFactory = new Configuration().configure()
				.addClass(Student.class).addClass(Teacher.class)
				.buildSessionFactory();
	}

	// 保存，有关联关系
	@Test
	public void testSave() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Teacher t1 = new Teacher();
		Teacher t2 = new Teacher();
		t1.setName("张老师");
		t2.setName("刘老师");
		Student s1 = new Student();
		Student s2 = new Student();
		s1.setName("王同学");
		s2.setName("李同学");
		t1.getStudents().add(s1);
		t1.getStudents().add(s2);
		t2.getStudents().add(s1);
		t2.getStudents().add(s2);
		s1.getTeachers().add(t1);
		s1.getTeachers().add(t2);
		s2.getTeachers().add(t1);
		s2.getTeachers().add(t2);
		session.save(t1);
		session.save(t2);
		System.out.println(s1.getId());
		session.save(s1);
		System.out.println(s1.getId());
		session.save(s2);
		session.getTransaction().commit();
		session.close();
	}

	// 获取，可以获取到关联的对方
	@Test
	public void testGet() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		Teacher teacher = (Teacher) session.get(Teacher.class, 1);
		System.out.println(teacher);
		System.out.println(teacher.getStudents());
		ts.commit();
		session.close();
	}

	@Test
	public void testRemoveRelation() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		Teacher teacher = (Teacher) session.get(Teacher.class, 2);
		teacher.getStudents().clear();
		ts.commit();
		session.close();
	}

	@Test
	public void testDelete() {
		// 分为有关联和无关联的两种情况
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		Teacher teacher = (Teacher) session.get(Teacher.class, 6);
		session.delete(teacher);
		ts.commit();
		session.close();
	}
}
