package com.tcwgq.s_second_cache;

import java.util.Iterator;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

public class App {
	private static SessionFactory sessionFactory = null;
	static {
		sessionFactory = new Configuration().configure()
				.addClass(Department.class).addClass(Employee.class)
				.buildSessionFactory();
	}

	@Test
	public void testSave() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Department department = new Department();
		department.setName("研发部");
		Employee e1 = new Employee();
		e1.setName("zhangSan");
		e1.setDepartment(department);
		Employee e2 = new Employee();
		e2.setName("liSi");
		e2.setDepartment(department);
		department.getEmployees().add(e1);
		department.getEmployees().add(e2);
		session.save(department);
		session.getTransaction().commit();
		session.close();
	}

	// 测试一级缓存，也叫session缓存
	@Test
	public void testSessionCache() {
		// 第一个session
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Department department = (Department) session.get(Department.class, 1);
		System.out.println(department.getName());
		Set<Employee> set = department.getEmployees();
		for (Employee emp : set) {
			System.out.println(emp.getId() + "--" + emp.getName() + "--"
					+ emp.getDepartment().getName());
		}
		session.getTransaction().commit();
		session.close();
		// 第二个session
		Session session2 = sessionFactory.openSession();
		session2.beginTransaction();
		Department department2 = (Department) session2.get(Department.class, 1);
		System.out.println(department2.getName());
		Set<Employee> set2 = department2.getEmployees();
		for (Employee emp : set2) {
			System.out.println(emp.getId() + "--" + emp.getName() + "--"
					+ emp.getDepartment().getName());
		}
		session2.getTransaction().commit();
		session2.close();
	}

	@Test
	public void testSecondCache() {
		// 第一个session
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Employee emp = (Employee) session.get(Employee.class, 1);
		System.out.println(emp.getName());
		session.getTransaction().commit();
		session.close();
		// 第二个session
		Session session2 = sessionFactory.openSession();
		session2.beginTransaction();
		Employee emp2 = (Employee) session2.get(Employee.class, 1);
		emp2.setName("科创部");
		System.out.println(emp2.getName());
		session2.getTransaction().commit();
		session2.close();
	}

	@Test
	public void testQueryCache() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Employee emp = (Employee) session.get(Employee.class, 1);
		System.out.println(emp.getName());
		session.getTransaction().commit();
		session.close();
	}

	@Test
	public void testQueryCache2() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Iterator<Employee> it = session.createQuery(
				"from Employee emp where emp.id < 15").iterate();
		while (it.hasNext()) {
			Employee emp = it.next();
			System.out.println(emp.getName());
		}
		Employee emp1 = (Employee) session.get(Employee.class, 1);
		System.out.println(emp1.getName());
		session.getTransaction().commit();
		session.close();
	}

}
