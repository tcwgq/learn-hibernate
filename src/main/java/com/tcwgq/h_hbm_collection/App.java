package com.tcwgq.h_hbm_collection;

import java.util.Arrays;
import java.util.TreeSet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

public class App {
	private static SessionFactory sessionFactory = null;
	static {
		sessionFactory = new Configuration().configure().addClass(User.class)
				.buildSessionFactory();
	}

	@Test
	public void testSave() {
		User user = new User();
		user.setName("张三");
		// Set<String> set = new HashSet<String>();
		// set.add("来广路一号");
		// set.add("北京路二号");
		// set.add("天安门广场");
		// user.setAddressSet(set);
		// Set集合初始化的好处
		// 当设置了sorted属性时，要使用SortedSet集合
		user.setAddressSet(new TreeSet<String>());
		user.getAddressSet().add("北京路5号");
		user.getAddressSet().add("上海路8号");
		user.getAddressSet().add("台湾路88号");
		user.getAddressList().add("aaaa");
		user.getAddressList().add("bbbb");
		user.getAddressList().add("cccc");
		user.setAddressArray(new String[] { "1234", "5678" });
		user.getAddressMap().put("山东科技大学", "276100");
		user.getAddressMap().put("北京大学", "65870");
		user.getAddressBag().add("hahahahah");
		user.getAddressBag().add("hehehhehe");
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(user);
		session.getTransaction().commit();
		session.close();
	}

	@Test
	public void testGet() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		User user = (User) session.get(User.class, 5);
		ts.commit();
		System.out.println(user.getAddressSet());
		System.out.println(user.getAddressList());
		System.out.println(Arrays.toString(user.getAddressArray()));
		System.out.println(user.getAddressMap());
		System.out.println(user.getAddressBag());
		// 此时需要注意session.close()需要放在最后
		session.close();
	}
}
