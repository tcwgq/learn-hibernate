package com.tcwgq.m_hbm_OneToOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

public class App {
	private static SessionFactory sessionFactory = null;
	static {
		sessionFactory = new Configuration().configure().addClass(Person.class)
				.addClass(IDCard.class).buildSessionFactory();
	}

	// 保存，有关联关系
	@Test
	public void testSave() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Person person = new Person();
		person.setName("zhangSan");
		IDCard idCard = new IDCard();
		idCard.setNumber("123456xxx");
		person.setIdCard(idCard);
		idCard.setPerson(person);
		session.save(person);
		session.save(idCard);
		session.getTransaction().commit();
		session.close();
	}

	// 获取，可以获取到关联的对方
	@Test
	public void testGet() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		// 先获取person
		Person person = (Person) session.get(Person.class, 1);
		System.out.println(person.getName());
		System.out.println(person.getIdCard().getNumber());
		// // 现获取idCard
		// IDCard idCard = (IDCard) session.get(IDCard.class, 1);
		// System.out.println(idCard.getNumber());
		// System.out.println(idCard.getPerson().getName());
		ts.commit();
		session.close();
	}

	// 基于主键的一对一关系，解除关联关系时，双方都不可以
	@Test
	public void testRemoveRelation() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		// 从无外键方解除关系不可以
		// Person person = (Person) session.get(Person.class, 1);
		// person.setIdCard(null);
		// 从有外键方解除关系也不可以
		IDCard idCard = (IDCard) session.get(IDCard.class, 1);
		idCard.setPerson(null);
		ts.commit();
		session.close();
	}

	// 基于主键的一对一关联关系，有外键方可以删除
	@Test
	public void testDelete() {
		// 分为有关联和无关联的两种情况
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		// 存在外键约束，不能删除
		// Person person = (Person) session.get(Person.class, 1);
		// session.delete(person);
		// 由本方维护关联关系，可以删除
		IDCard idCard = (IDCard) session.get(IDCard.class, 1);
		session.delete(idCard);
		ts.commit();
		session.close();
	}
}
